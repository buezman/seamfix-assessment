package com.buezman.bvnValidator.controller;

import com.buezman.bvnValidator.constants.BvnResponseCode;
import com.buezman.bvnValidator.constants.BvnResponseMessage;
import com.buezman.bvnValidator.entity.BvnData;
import com.buezman.bvnValidator.entity.BvnValidationEntry;
import com.buezman.bvnValidator.payload.BvnValidationRequest;
import com.buezman.bvnValidator.payload.BvnValidationResponse;
import com.buezman.bvnValidator.repository.BvnValidationEntryRepository;
import com.buezman.bvnValidator.service.MockBvnData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.time.Duration;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BvnValidationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private BvnValidationEntryRepository bvnValidationEntryRepository;

    private final String bvnValidationUrl = "/bv-service/svalidate/wrapper";


    @DisplayName("Test: Valid BVN in request payload")
    @Test
    public void whenValidateBvn_WithValidBvn_ShouldPass() throws Exception {
        String validBvn = "01234567890";
        BvnValidationRequest request = new BvnValidationRequest();
        request.setBvn(validBvn);

        Instant start = Instant.now();
        var response = mockMvc.perform(post(bvnValidationUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.message").value(BvnResponseMessage.SUCCESS))
                .andExpect(jsonPath("$.code").value(BvnResponseCode.SUCCESS))
                .andExpect(jsonPath("$.bvn").value(validBvn))
                .andReturn().getResponse().getContentAsString();
        Instant end = Instant.now();
        BvnValidationResponse validationResponse = objectMapper.readValue(response, BvnValidationResponse.class);
        assertTrue(validationResponse.getBasicDetail() != null && !validationResponse.getBasicDetail().isBlank());
        assertTrue(validationResponse.getImageDetail() != null && !validationResponse.getImageDetail().isBlank());
        assertTrue(Base64.isBase64(validationResponse.getBasicDetail()));
        assertTrue(Base64.isBase64(validationResponse.getImageDetail()));
        assertTrue(Duration.between(start, end).getSeconds() < 5);
        verify(bvnValidationEntryRepository, times(1)).save(any(BvnValidationEntry.class));

    }

    @DisplayName("Test: Empty BVN in request payload (null or blank)")
    @Test
    public void whenValidateBvn_WithEmptyOrBlankBvn_ShouldFail() throws Exception {
        BvnValidationRequest request = new BvnValidationRequest();
        request.setBvn("   ");

        Instant start = Instant.now();
        mockMvc.perform(post(bvnValidationUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value(BvnResponseMessage.EMPTY))
                .andExpect(jsonPath("$.code").value(BvnResponseCode.BAD_REQUEST));
        Instant end = Instant.now();

        verify(bvnValidationEntryRepository, times(1)).save(any(BvnValidationEntry.class));
        assertTrue(Duration.between(start, end).getSeconds() < 1);
    }

    @DisplayName("Test: Invalid BVN in request payload - Bvn Not Found")
    @Test
    public void whenValidateBvn_WithBvnNotFound_ShouldFail() throws Exception {
        String invalidBvn = "55555555555";
        BvnValidationRequest request = new BvnValidationRequest();
        request.setBvn(invalidBvn);

        Instant start = Instant.now();
        mockMvc.perform(post(bvnValidationUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value(BvnResponseMessage.NOT_FOUND))
                .andExpect(jsonPath("$.code").value(BvnResponseCode.NOT_FOUND))
                .andExpect(jsonPath("$.bvn").value(invalidBvn));
        Instant end = Instant.now();

        verify(bvnValidationEntryRepository, times(1)).save(any(BvnValidationEntry.class));
        assertTrue(Duration.between(start, end).getSeconds() < 1);

    }

    @DisplayName("Test: Invalid BVN (Less than 11 BVN digits) in request payload")
    @Test
    public void whenValidateBvn_WithInvalidBvnCharacterLength_ShouldFail() throws Exception {
        String bvnWithInvalidLength = "123456789";
        BvnValidationRequest request = new BvnValidationRequest();
        request.setBvn(bvnWithInvalidLength);

        Instant start = Instant.now();
        mockMvc.perform(post(bvnValidationUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value(BvnResponseMessage.INVALID))
                .andExpect(jsonPath("$.code").value(BvnResponseCode.INVALID_BVN_DIGITS))
                .andExpect(jsonPath("$.bvn").value(bvnWithInvalidLength));
        Instant end = Instant.now();
        verify(bvnValidationEntryRepository, times(1)).save(any(BvnValidationEntry.class));
        assertTrue(Duration.between(start, end).getSeconds() < 1);
    }

    @DisplayName("Test: Invalid BVN (Contains non digits) in request payload")
    @Test
    public void whenValidateBvn_ContainingNonDigit_ShouldFail() throws Exception {
        String bvnWithNonDigit = "012345E6789";
        BvnValidationRequest request = new BvnValidationRequest();
        request.setBvn(bvnWithNonDigit);

        Instant start = Instant.now();
        mockMvc.perform(post(bvnValidationUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value(BvnResponseMessage.INVALID))
                .andExpect(jsonPath("$.code").value(BvnResponseCode.BAD_REQUEST))
                .andExpect(jsonPath("$.bvn").value(bvnWithNonDigit));
        Instant end = Instant.now();
        verify(bvnValidationEntryRepository, times(1)).save(any(BvnValidationEntry.class));
        assertTrue(Duration.between(start, end).getSeconds() < 1);

    }

    private BvnData getValidBvnData() {
        return MockBvnData.MOCK_BVN_DATASET.stream()
                .findAny().orElse(null);
    }
}