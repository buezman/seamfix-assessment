package com.buezman.bvnValidator.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BvnData {
    private String bvn;
    private String basicDetail;
    private String imageDetail;
}
