package com.buezman.bvnValidator.entity;

import com.buezman.bvnValidator.payload.BvnValidationRequest;
import com.buezman.bvnValidator.payload.BvnValidationResponse;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document
public class BvnValidationEntry {
    @Id
    private String id;
    private BvnValidationRequest request;
    private BvnValidationResponse response;
}
