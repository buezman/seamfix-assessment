package com.buezman.bvnValidator.constants;

public class BvnResponseMessage {
    public static String SUCCESS = "Success";
    public static String NOT_FOUND = "The searched BVN does not exist";
    public static String EMPTY = "One or more of your request parameters failed validation. Please retry";
    public static String INVALID = "The searched BVN is invalid";
}
