package com.buezman.bvnValidator.repository;

import com.buezman.bvnValidator.entity.BvnValidationEntry;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BvnValidationEntryRepository extends MongoRepository<BvnValidationEntry, String> {
}
