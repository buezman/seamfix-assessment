package com.buezman.bvnValidator.controller;

import com.buezman.bvnValidator.payload.BvnValidationRequest;
import com.buezman.bvnValidator.payload.BvnValidationResponse;
import com.buezman.bvnValidator.service.BvnValidationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/bv-service/svalidate")
public class BvnValidationController {

    private final BvnValidationService bvnValidationService;

    @PostMapping("/wrapper")
    public ResponseEntity<BvnValidationResponse> validateBvn(@RequestBody BvnValidationRequest request) {
        return bvnValidationService.validateBvn(request);
    }

}
