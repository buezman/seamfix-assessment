package com.buezman.bvnValidator.service;

import com.buezman.bvnValidator.entity.BvnValidationEntry;
import com.buezman.bvnValidator.payload.BvnValidationRequest;
import com.buezman.bvnValidator.payload.BvnValidationResponse;
import org.springframework.http.ResponseEntity;

public interface BvnValidationService {
    ResponseEntity<BvnValidationResponse> validateBvn(BvnValidationRequest request);
    void saveValidationEntry(BvnValidationEntry entry);
}
