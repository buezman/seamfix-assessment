package com.buezman.bvnValidator.service;

import com.buezman.bvnValidator.entity.BvnData;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Service
public class MockBvnData {
    public static final List<BvnData> MOCK_BVN_DATASET = List.of(
        buildBvnData("01234567890"),
        buildBvnData("11234567890"),
        buildBvnData("21234567890"),
        buildBvnData("31234567890"),
        buildBvnData("41234567890")
    );

    private static BvnData buildBvnData(String bvn) {
        String encodedString = Base64.getEncoder().encodeToString(bvn.getBytes(StandardCharsets.UTF_8));
        return BvnData.builder()
                .bvn(bvn)
                .imageDetail(encodedString)
                .basicDetail(encodedString)
                .build();
    }

    public Optional<BvnData> findByBvn(String bvn) {
        return MOCK_BVN_DATASET.stream()
                .filter(bvnData -> bvnData.getBvn().equals(bvn))
                .findFirst();
    }


}
