package com.buezman.bvnValidator.service;

import com.buezman.bvnValidator.constants.BvnResponseCode;
import com.buezman.bvnValidator.constants.BvnResponseMessage;
import com.buezman.bvnValidator.entity.BvnData;
import com.buezman.bvnValidator.entity.BvnValidationEntry;
import com.buezman.bvnValidator.payload.BvnValidationRequest;
import com.buezman.bvnValidator.payload.BvnValidationResponse;
import com.buezman.bvnValidator.repository.BvnValidationEntryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Slf4j
@Service
@RequiredArgsConstructor
public class BvnValidationServiceImpl implements BvnValidationService {

    private final BvnValidationEntryRepository bvnValidationEntryRepository;
    private final MockBvnData mockBvnData;

    @Override
    public ResponseEntity<BvnValidationResponse> validateBvn(BvnValidationRequest request) {
        String bvn = request.getBvn();
        BvnValidationResponse response;
        HttpStatus status = HttpStatus.BAD_REQUEST;

        if (bvn == null || bvn.isBlank()) {
            response = BvnValidationResponse.builder()
                    .message(BvnResponseMessage.EMPTY)
                    .code(BvnResponseCode.BAD_REQUEST)
                    .bvn(bvn)
                    .build();
        } else if (!bvn.matches("[0-9]+")) {
            response = BvnValidationResponse.builder()
                    .message(BvnResponseMessage.INVALID)
                    .code(BvnResponseCode.BAD_REQUEST)
                    .bvn(bvn)
                    .build();
        } else if (bvn.length() != 11) {
            response = BvnValidationResponse.builder()
                    .message(BvnResponseMessage.INVALID)
                    .code(BvnResponseCode.INVALID_BVN_DIGITS)
                    .bvn(bvn)
                    .build();
        } else {
            BvnData bvnData = mockBvnData.findByBvn(bvn).orElse(null);
            if (bvnData == null) {
                response = BvnValidationResponse.builder()
                        .message(BvnResponseMessage.NOT_FOUND)
                        .code(BvnResponseCode.NOT_FOUND)
                        .bvn(bvn)
                        .build();
                status = HttpStatus.NOT_FOUND;
            } else {
                response = BvnValidationResponse.builder()
                        .message(BvnResponseMessage.SUCCESS)
                        .code(BvnResponseCode.SUCCESS)
                        .bvn(request.getBvn())
                        .imageDetail(bvnData.getImageDetail())
                        .basicDetail(bvnData.getBasicDetail())
                        .build();
                status = HttpStatus.OK;
            }
        }
        BvnValidationEntry validationEntry = BvnValidationEntry.builder()
                .request(request)
                .response(response)
                .build();
        saveValidationEntry(validationEntry);

        return new ResponseEntity<>(response, status);
    }

    @Override
    @Async("threadPoolTaskExecutor")
    public void saveValidationEntry(BvnValidationEntry entry) {
        CompletableFuture.runAsync(() -> bvnValidationEntryRepository.save(entry));
    }

}
