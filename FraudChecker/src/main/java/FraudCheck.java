import java.util.*;

public class FraudCheck {
    public static void main(String[] args) {
        String data1 = "5 2\n3 6 7 1 3"; //0
        String data2 = "3 5\n2 3 4 "; //0
        String data3 = "9 5\n2 3 4 2 3 6 4 8 4 5"; //2
        String data4 = "10 4\n1 3 2 3 6 4 5 3 2 7"; //2
        String data5 = "11 3\n1 3 2 5 6 4 11 5 3 2 7"; //4
        String data6 = "8 3\n2 2 4 13 16 25 40 51"; //4
        System.out.println(countFraudNotifications(data1));
        System.out.println(countFraudNotifications(data2));
        System.out.println(countFraudNotifications(data3));
        System.out.println(countFraudNotifications(data4));
        System.out.println(countFraudNotifications(data5));
        System.out.println(countFraudNotifications(data6));
    }

    public static int countFraudNotifications(String data) {

        String[] arr = data.split("\n");
        String[] line1 = arr[0].split(" ");
        int n = Integer.parseInt(line1[0]);
        int d = Integer.parseInt(line1[1]);
        int notificationCount = 0;

        if (d >= n) return notificationCount;

        String[] expenditures = arr[1].split(" ");

        List<String> priorExpenditures = getSublist(expenditures, d);
        for (int i = d; i < n; i++ ) {
            double currentSpend = Integer.parseInt(expenditures[i]);
            double priorExpensesMedian = getMedian(priorExpenditures);
            if (currentSpend >= 2 * priorExpensesMedian) notificationCount++;
            priorExpenditures.remove(0);
            priorExpenditures.add(expenditures[i]);
        }
        return notificationCount;
    }

    private static double getMedian(List<String> list) {
        List<String> sorted = list.stream().sorted(Comparator.comparingInt(Integer::parseInt)).toList();
        int mid = list.size()/2;
        if (sorted.size() % 2 == 0) {
            return (Integer.parseInt(sorted.get(mid)) + Integer.parseInt(sorted.get(mid-1)))/2.0;
        } else {
            return Integer.parseInt(sorted.get(mid));
        }
    }

    private static List<String> getSublist(String[] arr, int d) {
        return new ArrayList<>(Arrays.asList(arr).subList(0, d));
    }
}

